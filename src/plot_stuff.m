%% PTC3451-T6comp Solução da Parte Computacional do Teste 6 de PTC3451
%% Copyright (C) 2021  João Pedro de Omena Simas

%% This program is free software: you can redistribute it and/or modify
%% it under the terms of the GNU General Public License as published by
%% the Free Software Foundation, either version 3 of the License, or
%% (at your option) any later version.

%% This program is distributed in the hope that it will be useful,
%% but WITHOUT ANY WARRANTY; without even the implied warranty of
%% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%% GNU General Public License for more details.

%% You should have received a copy of the GNU General Public License
%% along with this program.  If not, see <https://www.gnu.org/licenses/>.

graphics_toolkit("qt")

%% read data
X = csvread("../out/x.csv");
Y = csvread("../out/y.csv");
states = csvread("../out/states.csv");
statesAPriori = csvread("../out/statesAPriori.csv");

%% plot y
figure;
hold on;

title("$\\mathbf{a}(n)$");
xlabel("iteração");

plot(X(1,:), "+");
print -dtikz ../img/x_sig.pgf

%% plot y
figure;
hold on;

title("$\\mathbf{y}(n)$")
xlabel("iteração");

plot(Y, "+");
print -dtikz ../img/y_sig.pgf

%% plot states
%% a posteriori
figure;
hold on;

for i = 1:size(X)(1)
  subplot(ceil(size(X)(1)/2), 2, i);
  plot(states(i,:), "+");
  %%plot(X(i,:), "r+");
  ylim([-3 3]);
  title(strcat("$\\widehat{x}_", int2str(i),"(n|n)$"))
endfor

print -dtikz ../img/x_post.pgf

%% a priori
figure;
hold on;

for i = 1:size(X)(1)
  subplot(ceil(size(X)(1)/2), 2, i);
  plot(statesAPriori(i,:), "+");
  %%plot(X(i,:), "r+");
  ylim([-3 3]);
  title(strcat("$\\widehat{x}_", int2str(i),"(n|n - 1)$"))
endfor

print -dtikz ../img/x_pri.pgf
  
%% plot errors
%% a posteriori
figure;
hold on;


for i = 1:size(X)(1)
  subplot(ceil(size(X)(1)/2), 2, i);
  plot(X(i,:) - states(i,:), "+");
  ylim([-3 3]);
  title(strcat("$\\varepsilon_", int2str(i),"(n|n)$"))
endfor

print -dtikz ../img/e_post.pgf

%% a priori
figure;
hold on;

for i = 1:size(X)(1)
  subplot(ceil(size(X)(1)/2), 2, i);
  plot(X(i,:) - statesAPriori(i,:), "+");
  ylim([-3 3]);
  title(strcat("$\\varepsilon_", int2str(i),"(n|n - 1)$"))
endfor

print -dtikz ../img/e_pri.pgf
