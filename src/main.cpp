// PTC3451-T6comp Solução da Parte Computacional do Teste 6 de PTC3451
// Copyright (C) 2021  João Pedro de Omena Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <iostream>
#include <random>
#include <fstream>

#include <eigen3/Eigen/Dense>

#include "Kalman.hh"

template<typename Matr_t>
void saveMatrix(std::string fname, const Matr_t& matr) {
  std::ofstream fileOut;
  fileOut.open(fname);
  for(auto i = 0; i < matr.rows(); i++) {
    for(auto j = 0; j < matr.cols(); j++)
      fileOut << matr(i, j) << ((j != (matr.cols() - 1))? ", " : "");
    fileOut << std::endl;
  }
}

int main() {

  std::cout << 
    "PTC3451-T6comp  Copyright (C) 2021  João Pedro de Omena Simas\n"
    "This program comes with ABSOLUTELY NO WARRANTY.\n"
    "This is free software, and you are welcome to redistribute it\n"
    "under certain conditions.\n";
  
  constexpr size_t NSamples = 500;
  constexpr size_t Nx = 3;
  constexpr size_t Ny = 1;

  using T = float;

  // Matrix de 3x3 de transição do processo x(i)
  const auto A = [](){
    Eigen::Matrix<T, Nx, Nx> ret;
    ret <<
      0, 0, 0,
      1, 0, 0,
      0, 1, 0;
    return ret;
  }();

  // Matrix de 1x3 de observação (H)
  const auto H = [](){
    Eigen::Matrix<T, Ny, Nx> ret;
    ret << 0.25, 1, 0.25;
    return ret;
  }();

  // Matriz 3x3 de covariância do ruído do processo x(i)
  const auto SigmaNx = [](){
    Eigen::Matrix<T, Nx, Nx> ret;
    ret <<
      1, 0, 0,
      0, 0, 0,
      0, 0, 0;
    return ret;
  }();

  // variância do ruído da entrada (y(i)) real
  constexpr T varNY = 0.01;
  constexpr T sigmaNy = std::sqrt(varNY);

  // variância do ruído da entrada (y(i)) estimada
  constexpr T varNYEst = 0.02;
  constexpr T sigmaNyEst = std::sqrt(varNYEst);

  // Matriz 1x1 de covariância do ruído da entrada (y(i))
  const auto SigmaNy = [sigmaNyEst](){
    Eigen::Matrix<T, Ny, Ny> ret;
    ret << std::sqrt(sigmaNyEst);
    return ret;
  }();

  // gera sequência binária aleatória a(i)
  const auto a = [Nx](){
    std::random_device rd;
    std::mt19937 gen(rd());
    std::bernoulli_distribution distB;
    
    Eigen::Matrix<T, NSamples + Nx - 1, 1> a;

    for(auto i = 0; i < a.rows(); i++)
      a[i] = 2.0f*distB(gen) - 1.0f;

    return a;
  }();

  // cria sequência de vetores entrada do filtro
  const auto X = [](const Eigen::Matrix<T, NSamples + Nx - 1, 1>& x){
    Eigen::Matrix<T, Nx, NSamples> X;
    for(auto i = 0; i < X.cols(); i++) 
      X.col(i) = x.segment<Nx>(i).reverse();

    return X;
  }(a);


  // objeto do filtro de Kalman
  Kalman<T, Nx, Ny> kalman(A, H, SigmaNx, SigmaNy);

  // vetores de estado e de saída do filtro
  Eigen::Matrix<T, Nx, NSamples> states;
  Eigen::Matrix<T, Nx, NSamples> statesAPriori;
  Eigen::Matrix<T, Ny, NSamples> Y;

  // roda simulação
  {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::normal_distribution distN;

    const auto b = Eigen::Matrix<T, Ny, 1>::Identity();
    
    for(auto i = 0; i < NSamples; i++){
      const Eigen::Matrix<T, Nx, 1>& xi = X.col(i);
      // gera y(i) 
      Eigen::Matrix<T, Ny, 1> yi = H*xi + sigmaNy*distN(gen)*b;
      auto [xHatI, xHatPrioriI] = kalman.update(yi);

      states.col(i) = xHatI;
      statesAPriori.col(i) = xHatPrioriI;
      Y.col(i) = yi;
    }
  }

  // salva resultados
  saveMatrix("../out/x.csv", X);
  saveMatrix("../out/y.csv", Y);  
  saveMatrix("../out/states.csv", states);
  saveMatrix("../out/statesAPriori.csv", statesAPriori);
  
  return 0;
}
