// PTC3451-T6comp Solução da Parte Computacional do Teste 6 de PTC3451
// Copyright (C) 2021  João Pedro de Omena Simas

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <eigen3/Eigen/Dense>
template<typename T, size_t Nx, size_t Ny>
class Kalman {

public:
  using VecX = Eigen::Matrix<T, Nx, 1>;
  using VecY = Eigen::Matrix<T, Ny, 1>;
  using MatXX = Eigen::Matrix<T, Nx, Nx>;
  using MatYY = Eigen::Matrix<T, Ny, Ny>;
  using MatXY = Eigen::Matrix<T, Nx, Ny>;
  using MatYX = Eigen::Matrix<T, Ny, Nx>;
  
  Kalman(const MatXX& A, const MatYX& H, const MatXX& SigmaNx, const MatYY& SigmaNy, T delta = 10);
  void init();
  
  std::pair<VecX, VecX> update(const VecY& y);
  std::pair<VecX, VecX> getState() {return std::make_pair(xHat, xHatPriori);};
private:
  MatXX A, SigmaNx, P;
  MatYY SigmaNy;
  MatYX H;
  MatXY K;

  VecX xHat, xHatPriori;

  T delta;
};

template<typename T, size_t Nx, size_t Ny>
Kalman<T, Nx, Ny>::Kalman(const MatXX& A, const MatYX& H, const MatXX& SigmaNx,
	       const MatYY& SigmaNy, T delta)
  : A(A), H(H), SigmaNx(SigmaNx), SigmaNy(SigmaNy), delta(delta),
    xHat(decltype(xHat)::Zero()),
    xHatPriori(decltype(xHatPriori)::Zero()),
    P(delta*decltype(P)::Identity()) {
}

template<typename T, size_t Nx, size_t Ny>
void Kalman<T, Nx, Ny>::init() {
  xHat.setZero();
  P = delta*decltype(P)::Identity();
}

template<typename T, size_t Nx, size_t Ny>
std::pair<typename Kalman<T, Nx, Ny>::VecX, typename Kalman<T, Nx,
Ny>::VecX> Kalman<T, Nx, Ny>::update(const VecY& y) {
  xHatPriori = A*xHat;
  P = A*P*A.transpose() + SigmaNx;
  
  K = P*H.transpose()*(H*P*H.transpose() + SigmaNy).inverse();

  xHat = xHatPriori + K*(y - H*xHatPriori);
  
  P -= K*H*P;

  return std::make_pair(xHat, xHatPriori);
}

